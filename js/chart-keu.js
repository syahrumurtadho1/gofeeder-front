var stackedBar = {
  responsive: true,
  legend: {
    display: false,
  },

  tooltips: {
    enabled: false,
  },

  cornerRadius: 8,

  scales: {
    xAxes: [
      {
        scaleLabel: {
          display: false,
        },
        gridLines: {
          display: false,
        },
        ticks: {
          display: false,
        },
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
          },
        },

        stacked: true,
      },
    ],
    yAxes: [
      {
        afterFit: function (scale) {
          scale.width = 0;
        },
        gridLines: {
          display: false,
        },

        gridLines: {
          display: false,
        },
        ticks: {
          display: false,
        },

        stacked: true,
      },
    ],
  },
  legend: {
    display: false,
  },
};

var barChart = {
  responsive: true,
  legend: {
    display: false,
  },
  tooltips: {
    xPadding: 12,
    yPadding: 12,
    bodyFontFamily: "'Poppins', sans-serif",
    backgroundColor: "#37375C",
    position: "average",
    // yAlign: "bottom"
  },
  cornerRadius: 2,
  scales: {
    xAxes: [
      {
        gridLines: {
          display: false,
          color: "#F5F6F8",
        },
      },
    ],
    yAxes: [
      {
        gridLines: {
          color: "#F5F6F8",
          // borderDash: [3, 5],
        },
        position: "left",
        //   id: "yaxis_1",
        ticks: {
          callback: function (value, index, values) {
            return "Rp " + value / 1000000 + " jt";
          },
        },
      },
    ],
  },
};

var horizontalBarChart = {
  responsive: true,
  legend: {
    display: false,
  },
  tooltips: {
    xPadding: 12,
    yPadding: 12,
    bodyFontFamily: "'Poppins', sans-serif",
    backgroundColor: "#37375C",
    position: "average",
    // yAlign: "bottom"
  },

  cornerRadius: 4,
  scales: {
    xAxes: [
      {
        gridLines: {
          display: true,
          color: "#F5F6F8",
        },
        ticks: {
          callback: function (value, index, values) {
            return "Rp " + value / 1000000 + " jt";
          },
        },
      },
    ],
    yAxes: [
      {
        gridLines: {
          display: false,
          color: "#F5F6F8",
        },
        position: "left",
        //   id: "yaxis_1",
        ticks: {
          // stepSize: 20,
        },
      },
    ],
  },
};

// Chart Tagihan

var ctx = document.getElementById("chart-tagihan").getContext("2d");

var gradient1 = ctx.createLinearGradient(0, 24, 0, 0);
gradient1.addColorStop(0, "rgba(0, 205, 172, 1)");
gradient1.addColorStop(1, "rgba(141, 218, 213, 1)");

var gradient2 = ctx.createLinearGradient(0, 0, 0, 24);
gradient2.addColorStop(0, "rgba(246, 211, 101, 1)");
gradient2.addColorStop(1, "rgba(253, 160, 133, 1)");

var chart1 = new Chart(ctx, {
  type: "horizontalBar",
  data: {
    datasets: [
      {
        data: [3326606615 / (3326606615 + 895480667)],
        backgroundColor: gradient1,
        hoverBackgroundColor: gradient1,
      },

      {
        data: [895480667 / (3326606615 + 895480667)],
        backgroundColor: gradient2,
        hoverBackgroundColor: gradient2,
      },
    ],
  },

  options: stackedBar,
});

// Chart Pembayaran Masuk
var ctx = document.getElementById("pembayaran-masuk").getContext("2d");

var gradient = ctx.createLinearGradient(0, 100, 0, 300);
gradient.addColorStop(0, "rgba(0, 205, 172, 0.35)");
gradient.addColorStop(1, "rgba(141, 218, 213, 0.35)");

var gradientHover = ctx.createLinearGradient(0, 100, 0, 300);
gradientHover.addColorStop(0, "rgba(0, 205, 172, 1)");
gradientHover.addColorStop(1, "rgba(141, 218, 213, 1)");

var chart2 = new Chart(ctx, {
  type: "bar",
  data: {
    labels: ["Sabtu", "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat"],
    datasets: [
      {
        backgroundColor: gradient,
        hoverBackgroundColor: gradientHover,
        type: "bar",
        data: [
          27456789,
          5123687,
          35159657,
          57619845,
          69123458,
          87987535,
          48654705,
        ],
        // yAxisID: "yaxis_1",
      },
    ],
  },
  options: barChart,
});

// Chart Pembayaran Masuk Filled
var ctx = document.getElementById("pembayaran-masuk_2").getContext("2d");

var gradient = ctx.createLinearGradient(0, 100, 0, 300);
gradient.addColorStop(0, "rgba(0, 205, 172, 0.35)");
gradient.addColorStop(1, "rgba(141, 218, 213, 0.35)");

var gradientHover = ctx.createLinearGradient(0, 100, 0, 300);
gradientHover.addColorStop(0, "rgba(0, 205, 172, 1)");
gradientHover.addColorStop(1, "rgba(141, 218, 213, 1)");

var chart2_2 = new Chart(ctx, {
  type: "bar",
  data: {
    labels: [
      "1 Feb",
      "2 Feb",
      "3 Feb",
      "4 Feb",
      "5 Feb",
      "6 Feb",
      "7 Feb",
      "8 Feb",
      "9 Feb",
      "10 Feb",
      "11 Feb",
      "12 Feb",
      "13 Feb",
      "14 Feb",
      "15 Feb",
      "16 Feb",
      "17 Feb",
      "18 Feb",
      "19 Feb",
      "20 Feb",
      "21 Feb",
      "22 Feb",
      "23 Feb",
      "24 Feb",
      "25 Feb",
      "26 Feb",
      "27 Feb",
      "28 Feb",
    ],
    datasets: [
      {
        backgroundColor: gradient,
        hoverBackgroundColor: gradientHover,
        type: "bar",
        data: [
          87987535,
          69123458,
          48654705,
          5123687,
          87987535,
          57619845,
          48654705,
          35159657,
          35159657,
          69123458,
          48654705,
          5123687,
          5123687,
          57619845,
          5123687,
          87987535,
          57619845,
          27456789,
          69123458,
          48654705,
          27456789,
          87987535,
          57619845,
          35159657,
          27456789,
          35159657,
          69123458,
          27456789,
        ],
        // yAxisID: "yaxis_1",
      },
    ],
  },
  options: barChart,
});

// Chart Komposisi Pembayaran Masuk

var ctx = document.getElementById("chart-komposisi").getContext("2d");

var chart3 = new Chart(ctx, {
  type: "horizontalBar",
  data: {
    labels: [
      "Pendaftaran",
      "PPS",
      "Himpunan",
      "Dana Pengembangan Pembangunan",
      "Operasional Semester",
      "Perpustakaan",
      "Registrasi",
      "Daftar Ulang",
      "Praktek Kerja Lapangan",
      "Karya Wisata Mahasiswa",
    ],
    datasets: [
      {
        data: [
          12654865,
          8567812,
          3578456,
          15521158,
          22458756,
          3578452,
          45815654,
          6548124,
          18458695,
          33578548,
        ],
        backgroundColor: "rgba(192, 57, 43, 0.35)",
        hoverBackgroundColor: "rgba(192, 57, 43, 1)",
      },
    ],
  },
  options: horizontalBarChart,
});

// CornerRadius

Chart.elements.Rectangle.prototype.draw = function () {
  function t(t) {
    return s[(f + t) % 4];
  }
  var r,
    e,
    i,
    o,
    _,
    h,
    l,
    a,
    b = this._chart.ctx,
    d = this._view,
    n = d.borderWidth,
    u = this._chart.config.options.cornerRadius;
  if (
    (u < 0 && (u = 0),
    void 0 === u && (u = 0),
    d.horizontal
      ? ((r = d.base),
        (e = d.x),
        (i = d.y - d.height / 2),
        (o = d.y + d.height / 2),
        (_ = e > r ? 1 : -1),
        (h = 1),
        (l = d.borderSkipped || "left"))
      : ((r = d.x - d.width / 2),
        (e = d.x + d.width / 2),
        (i = d.y),
        (_ = 1),
        (h = (o = d.base) > i ? 1 : -1),
        (l = d.borderSkipped || "bottom")),
    n)
  ) {
    var T = Math.min(Math.abs(r - e), Math.abs(i - o)),
      v = (n = n > T ? T : n) / 2,
      g = r + ("left" !== l ? v * _ : 0),
      c = e + ("right" !== l ? -v * _ : 0),
      C = i + ("top" !== l ? v * h : 0),
      w = o + ("bottom" !== l ? -v * h : 0);
    g !== c && ((i = C), (o = w)), C !== w && ((r = g), (e = c));
  }
  b.beginPath(),
    (b.fillStyle = d.backgroundColor),
    (b.strokeStyle = d.borderColor),
    (b.lineWidth = n);
  var s = [
      [r, o],
      [r, i],
      [e, i],
      [e, o],
    ],
    f = ["bottom", "left", "top", "right"].indexOf(l, 0);
  -1 === f && (f = 0);
  var q = t(0);
  b.moveTo(q[0], q[1]);
  for (var m = 1; m < 4; m++)
    (q = t(m)),
      (nextCornerId = m + 1),
      4 == nextCornerId && (nextCornerId = 0),
      (nextCorner = t(nextCornerId)),
      (width = s[2][0] - s[1][0]),
      (height = s[0][1] - s[1][1]),
      (x = s[1][0]),
      (y = s[1][1]),
      (a = u) > Math.abs(height) / 2 && (a = Math.floor(Math.abs(height) / 2)),
      a > Math.abs(width) / 2 && (a = Math.floor(Math.abs(width) / 2)),
      height < 0
        ? ((x_tl = x),
          (x_tr = x + width),
          (y_tl = y + height),
          (y_tr = y + height),
          (x_bl = x),
          (x_br = x + width),
          (y_bl = y),
          (y_br = y),
          b.moveTo(x_bl + a, y_bl),
          b.lineTo(x_br - a, y_br),
          b.quadraticCurveTo(x_br, y_br, x_br, y_br - a),
          b.lineTo(x_tr, y_tr + a),
          b.quadraticCurveTo(x_tr, y_tr, x_tr - a, y_tr),
          b.lineTo(x_tl + a, y_tl),
          b.quadraticCurveTo(x_tl, y_tl, x_tl, y_tl + a),
          b.lineTo(x_bl, y_bl - a),
          b.quadraticCurveTo(x_bl, y_bl, x_bl + a, y_bl))
        : width < 0
        ? ((x_tl = x + width),
          (x_tr = x),
          (y_tl = y),
          (y_tr = y),
          (x_bl = x + width),
          (x_br = x),
          (y_bl = y + height),
          (y_br = y + height),
          b.moveTo(x_bl + a, y_bl),
          b.lineTo(x_br - a, y_br),
          b.quadraticCurveTo(x_br, y_br, x_br, y_br - a),
          b.lineTo(x_tr, y_tr + a),
          b.quadraticCurveTo(x_tr, y_tr, x_tr - a, y_tr),
          b.lineTo(x_tl + a, y_tl),
          b.quadraticCurveTo(x_tl, y_tl, x_tl, y_tl + a),
          b.lineTo(x_bl, y_bl - a),
          b.quadraticCurveTo(x_bl, y_bl, x_bl + a, y_bl))
        : (b.moveTo(x + a, y),
          b.lineTo(x + width - a, y),
          b.quadraticCurveTo(x + width, y, x + width, y + a),
          b.lineTo(x + width, y + height - a),
          b.quadraticCurveTo(x + width, y + height, x + width - a, y + height),
          b.lineTo(x + a, y + height),
          b.quadraticCurveTo(x, y + height, x, y + height - a),
          b.lineTo(x, y + a),
          b.quadraticCurveTo(x, y, x + a, y));
  b.fill(), n && b.stroke();
};
